# Daily Pay - React take home test

This is a solution to the [React take home test](https://github.com/dailypay/react-take-home-test/).  

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)

## Overview

### The challenge

You’re building a simple Hacker News ([https://news.ycombinator.com](https://news.ycombinator.com/)) clone. The app will fetch data from a public JSON API endpoint and display it in a simple viewer.

The viewer should have two panels; a left pane showing the top 10 stories; and a right panel displaying a preview of the selected story using an iframe. When a user clicks a story in the left panel, the right panel should switch to the selected story.

Please refer to this simple interactive mockup made in Uizard to illustrate this: https://app.uizard.io/p/de328bd3

### Screenshot

![](./screenshot.png)

### Links

- Solution URL: [https://gitlab.com/aitorjs/dailypay-challenge](https://gitlab.com/aitorjs/dailypay-challenge)

- Live Site URL: [https://dailypay-challenge.vercel.app/]https://dailypay-challenge.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- Flexbox
- Typescript
- [TailwindCSS](https://tailwindcss.org/) - CSS framework
- [Flowbyte](https://flowbite.com/) - TailwindCSS components
- [ESLint](https://eslint.org/) - JavaScript style guide, linter, and formatter
- [React](https://reactjs.org/) - JS library
- [Gitlab](https://gitlab.com/) - Git forks
- [Vercel](https://vercel.com/) - Frontend cloud

### What I learned

- Continue practising with CSS, flexbox, gridbox, tailwindcss, flowbite, typescript, reactjs, nextjs and deployment.
- I learn about vitest: how to configure and use it.
- I learn about React Testing Library making some tests to check that app is running as expected.
- Add a gitlab action to pass tests on every new commit.
- [Note] Use node v17.0.0 or up to pass tests. ```structuredClose``` was introduced in nodejs v.17.0.0.

- Any feedback is so welcome! :)


