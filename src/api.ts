import { Ballot } from '@/types';
import DATA from '../data.json'

const api = {
  ballot: {
    list: async (): Promise<Ballot[]> => {
      return DATA.items as Ballot[]
    }
  }
}

export default api;