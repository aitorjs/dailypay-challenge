import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import { afterEach, describe, expect, it } from 'vitest'

import data from '../../mockedData.json'
import React from 'react'
import { ClientComponent } from './ClientComponent'
import { Ballot } from '@/types'

describe('ClientComponent', () => {
  // const ballots = await api.ballot.list();
  const ballots: Ballot[] = data
  const ballotNames = ballots.map(ballot => ballot.title)
  const nominees = ballots.map(ballot => ballot.items)

  afterEach(cleanup)

  it('Should render with correct data', async () => {
    // assert
    render(<ClientComponent ballots={ballots} />)

    ballotNames.map(ballotName => screen.getAllByText(ballotName))
    nominees.map(nominee =>
      nominee.map(n => screen.getAllByText(n.title))
    )
  })

  it('Should can vote', async () => {
    // assert
    const vdom = render(<ClientComponent ballots={ballots} />)

    const nominees1: any = screen.getAllByText(nominees[0][0].title)
    fireEvent.click(nominees1[0])
    const nominees2 = screen.getAllByText(nominees[1][0].title)
    fireEvent.click(nominees2[0])
    const vote = screen.getAllByText("Submit Ballot")
    fireEvent.click(vote[0])

    // check selected ballot modal appears
    const modalStyles = vdom.container.querySelector("#defaultModal")?.className
    expect(modalStyles?.match(/\b hidden\b/)).toBe(null)

    // check all the selected ballot data is correct
    ballotNames.map((ballotName, _x) => {
      const regex = new RegExp(`${ballotName} is for[\\w\\W]*${nominees[_x][0].title}`, 'gm')
      const str = `${ballotName} is for</span><span className='font-medium'>${nominees[_x][0].title}`;
      const match = str.match(regex)
      expect(match).not.toBeNull()
    })
  })
})