import { render, screen } from '@testing-library/react'
import { describe, it } from 'vitest'

import Home from '../app/page'

describe('App', () => {
  it('Should render', async () => {
    const home = await Home()
    render(home)

    screen.getAllByText('AWARDS 2021')
  })
})