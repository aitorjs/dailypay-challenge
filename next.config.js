/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'variety.com',
        port: '',
        pathname: '/wp-content/uploads/**/**',
      },
    ],
  },
}

module.exports = nextConfig
